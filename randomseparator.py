import os,os.path
import numpy as np
from os.path import isfile, join
from os import listdir
import sys
import shutil
from shutil import copyfile
import random
import collections


folder = './participants/'
list = os.listdir(folder)
number_files = len(list)
print('number of files',number_files)

folder_t = './participants_test/'
directory_t = os.path.dirname(folder_t)
if not os.path.exists(directory_t):
    os.makedirs(directory_t)

folder_v = './participants_validation/'
directory_v = os.path.dirname(folder_v)
if not os.path.exists(directory_v):
    os.makedirs(directory_v)


for_training = 14
total = number_files - for_training
random_numbers = []
for i in range(1,for_training):
    num = random.randint(1,total)
    random_numbers.append(num)


for j in range(len(list)):
    copyfile(folder + list[j], folder_t + list[j])


for k in range(len(random_numbers)):
    val_file = random_numbers[k]
    shutil.move(folder_t + list[val_file], folder_v + list[val_file])
    #sometimes crashes but rerun again and it may not

